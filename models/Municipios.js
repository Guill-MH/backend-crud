const Sequelize = require('sequelize');
const db = require('../config/db');
const Estados = require('./Estados');

const Municipios = db.define('Municipios', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    nombre: {
        type: Sequelize.STRING(30),
        allowNull: false,
    },
    activo: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
    },
});

//asociación de pertenencia
Estados.belongsTo(Estados, {onDelete: 'CASCADE' });

//exportarlo
module.exports = Municipios;