const Sequelize = require('sequelize');
const db = require('../config/db');
const Localidades = require('./Localidades');

const Cliente = db.define('Cliente', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    nombre: {
        type: Sequelize.STRING(30),
        allowNull: false,
    }, 
    apellidos: {
        type: Sequelize.STRING(30),
        allowNull: false,
    },
    rfc: {
        type: Sequelize.STRING(10),
        allowNull: false,
    },
    direccion: [
        {localidad: {Localidades}},
        {calle: { type: Sequelize.STRING(20), allowNull: false,} },
        {numExterior: { type: Sequelize.INTEGER(10), allowNull: false, }},
        {numInterior: { type: Sequelize.INTEGER(10), allowNull: false, }},
    ],
    email: {
        type: Sequelize.STRING(30),
        allowNull: false,
    },
    telefono: {
        type: Sequelize.INTEGER(10), 
        allowNull: false,
    },
    activo: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
    },
});

//asociación de pertenencia
Localidades.belongsTo(Localidades, {onDelete: 'CASCADE' });

//exportarlo
module.exports = Cliente;