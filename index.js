const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');


const routes = require('./routes');
//importar nuestro archivo de conexión a la BD
const db = require('./config/db');
require('./models/Estados');
require('./models/Municipios');

//decirle a sequelize que sincronice los modelos
db.sync({alter: true}) // 
    .then(() => {
        console.log("BD Conectada");
    })
    .catch((error) => {
        console.log(error);
    });

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cors());

app.listen(5000);