const Cliente = require('../models/Cliente');
const Localidad = require('../models/Localidades');
const { res } = require("express");
const { Op } = require("sequelize");

//Agregar Cliente
exports.agregar = async(req, res, next) => {
    try {
        await Cliente.create(req.body);
        res.json({ mensaje: 'Cliente agregado'});
    } catch (error) {
        console.error(error);
        res.json({ mensaje: 'Erro, Cliente no agregado'});
        next();
    }
};

//listar Clientes
exports.listar = async(req, res, next) => {
    try {
        let filtro = req.query;
        if (req.query.q) {
            filtro = { nombre: { [Op.like]: `%${req.query.q}%` } };
        }
        const clientes = await Cliente.findAll({
            where: filtro,
            include: [
                {model: Localidad}
            ]
        });
        res.json(clientes);
    } catch (error) {
        console.error(error);
        res.json({ mensaje: 'Error al leer un cliente'});
        next(); 
    }
};
//mostrar / leer por id
exports.mostrar = async(req, res, next) => {
    try {
        const clientes = await Municipio.findByPk(req.params.id);
        if (!clientes) {
            res.status(404).json({ mensaje: 'Cliente no encontrado'});
        } else {
            res.json(clientes);
        }
    } catch (error) {
        res.status(503).json({ mensaje: 'Error al leer el Cliente'});
        console.error(error);
    }
};
//Actualizar
exports.actualizar = async(req, res, next) => {
    try {
        const clientes = await Cliente.findByPk(req.params.id);
        if (!clientes) {
            res.status(404).json({ mensaje: 'No se encontro el cliente'});
        } else {

            Object.keys(req.body).forEach((propiedad) => {
                clientes[propiedad] = req.body[propiedad];
            })
            await clientes.save();
            res.json({ mensaje: 'Cliente actualizado'});
        }
    } catch (error) {
        console.error(error);
        let errores = [];
        if (error.errores) {
            errores = error.errores.map((item) => ({
                campo: item.path,
                error: item.message,
            }));
        }
        res.json({ 
            error: true,
            message: 'Error al actualizar el cliente',
            errores,
        });
    }
};
//Eliminar
exports.eliminar = async(req, res, next) => {
    try {
        //Eliminar registro, por id
        const clientes = await Municipio.findByPk(req.params.id);
        if (!clientes) {
            res.status(404).json({ mensaje: 'No se encontro el cliente'});
        } else {
            await clientes.destroy();
            res.json({ mensaje: 'Se elimino el cliente'});
        }
    } catch (error) {
        res.status(503).json({ mensaje: 'Error al eliminar el cliente'});
    }
};

