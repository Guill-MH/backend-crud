const Localidad = require('../models/Localidad');
const Municipio = require('../models/Municipios');
const { res } = require("express");
const { Op } = require("sequelize");


//Agregar  localidades
exports.agregar = async(req, res, next) => {
    try {
        //crear una localidad con los datos recibidos
        await Localidad.create(req.body);
        res.json({ mensaje: 'Se agregó la localidad.'});
    } catch (error) {
        console.error(error);
        let errores = [];
        if (error.errores) {
            errores = error.errores.map((item) => ({
                campo: item.path,
                error: item.message,
            }));
        }
        res.json({ 
            error: true,
            message: 'Error al agegar la localidad',
            errores,
        });
        next();
    }
};

//listar localidades
exports.listar = async(req, res, next) => {
    try {
        let filtro = req.query;
        if (req.query.q) {
            filtro = { nombre: { [Op.like]: `%${req.query.q}%` } };
        }
        const municipios = await Municipio.findAll({
            where: filtro,
            include: [
                {model: Municipio }
            ]
        });
        res.json(municipios);
    } catch (error) {
        console.error(error);
        res.json({ mensaje: 'Error al leer localidad'});
        next(); 
    }
};

//mostrar / leer localidades
exports.mostrar = async(req, res, next) => {
    try {
        //Buscar el registro, por id
        const localidades = await Localidad.findByPk(req.params.id);
        if (!localidades) {
            res.status(404).json({ mensaje: 'No se encontro la localidad. '});
        } else {
            res.json(localidades);
        }
    } catch (error) {
        res.status(503).json({ mensaje: 'Error al leer la localidad'});
        console.error(error);
    }
};
//Actualizar
exports.actualizar = async(req, res, next) => {
    try {
        //Obtener el registro de la localidad desde la bd
        const localidades = await Localidad.findByPk(req.params.id);
        if (!localidades) {
            res.status(404).json({ mensaje: 'No se encontro la localidad. '});
        } else {
            /*actualizar la bd
            categorias.nombre = req.body.nombre;
            categorias.activo = req.body.activo;
            el resto de las propiedades
            Procesar las propiedades que vienen en el body*/

            Object.keys(req.body).forEach((propiedad) => {
                localidades[propiedad] = req.body[propiedad];
            })

            //guardar los cambios
            await localidades.save();
            res.json({ mensaje: 'El registro fue actualizado'});
        }
    } catch (error) {
        console.error(error);
        let errores = [];
        if (error.errores) {
            errores = error.errores.map((item) => ({
                campo: item.path,
                error: item.message,
            }));
        }
        res.json({ 
            error: true,
            message: 'Error al actualizar la localidad',
            errores,
        });
    }
};
//Eliminar categoria
exports.eliminar = async(req, res, next) => {
    try {
        //Eliminar registro, por id
        const localidades = await Localidad.findByPk(req.params.id);
        if (!localidades) {
            res.status(404).json({ mensaje: 'No se encontro la localidad. '});
        } else {
            await localidades.destroy();
            res.json({ mensaje: 'Se elimino la localidad. '});
        }
    } catch (error) {
        res.status(503).json({ mensaje: 'Error al eliminar la localidad.'});
    }
};