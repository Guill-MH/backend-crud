const Estado = require('../models/Estados');

//Agregar  estado
exports.agregar = async(req, res, next) => {
    try {
        //crear un estado con los datos recibidos
        await Estado.create(req.body);
        res.json({ mensaje: 'Se agregó el Estado.'});
    } catch (error) {
        console.error(error);
        let errores = [];
        if (error.errores) {
            errores = error.errores.map((item) => ({
                campo: item.path,
                error: item.message,
            }));
        }
        res.json({ 
            error: true,
            message: 'Error al agregar estado',
            errores,
        });
        next();
    }
};

//listar estado
exports.listar = async(req, res, next) => {
    try {
        //extraer la lista de estados
        const estados = await Estado.findAll({});
        res.json(estados);
    } catch (error) {
        console.error(error);
        res.json({ mensaje: 'Error al leer estados'});
        next();
    }
};

//mostrar / leer
exports.mostrar = async(req, res, next) => {
    try {
        //Buscar el registro, por id
        const estados = await Estado.findByPk(req.params.id);
        if (!estados) {
            res.status(404).json({ mensaje: 'No se encontro el estado. '});
        } else {
            res.json(estados);
        }
    } catch (error) {
        res.status(503).json({ mensaje: 'Error al leer  estado'});
        console.error(error);
    }
};
//Actualizar
exports.actualizar = async(req, res, next) => {
    try {
        //Obtener el estado por id
        const estados = await Estado.findByPk(req.params.id);
        if (!estados) {
            res.status(404).json({ mensaje: 'No se encontro. '});
        } else {
           

            Object.keys(req.body).forEach((propiedad) => {
                estados[propiedad] = req.body[propiedad];
            })

            //guardar los cambios
            await estados.save();
            res.json({ mensaje: 'El registro fue actualizado'});
        }
    } catch (error) {
        console.error(error);
        let errores = [];
        if (error.errores) {
            errores = error.errores.map((item) => ({
                campo: item.path,
                error: item.message,
            }));
        }
        res.json({ 
            error: true,
            message: 'Error al actualizar ',
            errores,
        });
    }
};
//Eliminar estado
exports.eliminar = async(req, res, next) => {
    try {
        //Eliminar registro, por id
        const estados = await Estados.findByPk(req.params.id);
        if (!estados) {
            res.status(404).json({ mensaje: 'No se encontro . '});
        } else {
            await estados.destroy();
            res.json({ mensaje: 'Se elimino . '});
        }
    } catch (error) {
        res.status(503).json({ mensaje: 'Error al eliminar.'});
    }
};
