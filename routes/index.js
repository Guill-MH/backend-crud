const express = require('express');
const router = express.Router();

const estadosController = require('../controllers/EstadosControllers');
const municipiosController = require('../controllers/MunicipiosControllers');
const clientesController = require('../controllers/ClienteControllers');


//definir las rutas
module.exports = function () {

    //post: agregar estado
    router.post('/estados', estadosController.agregar);
    //get: leer estado
    router.get('/estados', estadosController.listar);
    //Get: leer estado
    router.get('/estados/:id', estadosController.mostrar);
    //put: actualizar estado
    router.put('/estados/:id', estadosController.actualizar);
    //delete: Eliminar estado
    router.delete('/estados/:id', estadosController.eliminar);

    //post: agregar municipio
    router.post('/municipios', municipiosController.agregar);
    //get: leer municipio
    router.get('/municipios', municipiosController.listar);
    //Get: leer municipio
    router.get('/municipios/:id', municipiosController.mostrar);
    //put: actualizar municipio
    router.put('/municipios/:id', municipiosontroller.actualizar);
    //delete: Eliminar municipio
    router.delete('/municipios/:id', municipiosController.eliminar);

    //post: agregar Localidad
    router.post('/localidad', localidadesController.agregar);
    //get: leer Localidad
    router.get('/localidad', localidadesController.listar);
    //Get: leer Localidad
    router.get('/localidades/:id', localidadesController.mostrar);
    //put: actualizar Localidad
    router.put('/localidades/:id', localidadesController.actualizar);
    //delete: Eliminar Localidad
    router.delete('/localidades/:id', localidadesController.eliminar);

    
    //post: agregar cliente
    router.post('/cliente', clientesController.agregar);
    //get: leer cliente
    router.get('/cliente', clientesController.listar);
    //Get: leer cliente
    router.get('/clientes/:id', clientesController.mostrar);
    //put: actualizar cliente
    router.put('/clientes/:id', clientesController.actualizar);
    //delete: Eliminar cliente
    router.delete('/lclientes/:id', clientesController.eliminar);

    return router;
};
